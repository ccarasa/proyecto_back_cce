
function generateRandom (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateIBAN (min, max) {
  return "ES34 "+generateRandom (min, max)+" "+generateRandom (min, max)+" "+generateRandom (min, max)+" "+generateRandom (min, max);
}

module.exports.generateRandom = generateRandom;
module.exports.generateIBAN = generateIBAN;
