const bcrypt = require('bcrypt');

//Como la encriptación consume recursos, en producción en asincrono y hay que manejar lamdas
//Nosotros vamos a lanzar sincrono aunque normalmente sería asincrono
function hash (data) {
  console.log("Hashing Data");
  return bcrypt.hashSync(data,10);
}

//sentPassword pasword recuperda en claro de la petición
function checkPassword (sentPassword, userHashedPassword) {
  console.log("Checking passwords");
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
