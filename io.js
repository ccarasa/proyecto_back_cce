//los formafor llegan en formato javascript y hay que pasar a json
//Persistencia a fichero utiliza la libreria FS
const fs = require('fs');

// Escritura a fichero
function writeUserDataToFile(data) {
  //Paso a formato JSON
  var jsonUserData = JSON.stringify(data);
  //Escritura a fichero
  // Parametros fichero, formato, enconding, gestión de error
  fs.writeFile("./users_pwd.json",jsonUserData, "utf8",
    function(err){ // los errores sueles ser por permisos de acceso s disco
      if (err){
        console.log("Error; "+err);
      }else{
        console.log("Usuarios escritos a Fichero.");
      }
    }
  );
}

module.exports.writeUserDataToFile = writeUserDataToFile;
