// VARIABLES Con 'CONST' por librerías
// Las variables que representan cargas de librería o nde configuración de fw se ponen con const
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

//Libreria para user JSON como formato del body de la respuesta
const bodyParser = require ('body-parser');
app.use(bodyParser.json());

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountsController = require('./controllers/AccountsController');
const externalAPIsController = require('./controllers/ExternalAPIsController');

app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto OPENSHIFT !!! en el Puerto:" + port);

// GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
  function(req, res){ // funcion manejadora
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechU AUTOMATICO !!!!"});
  }
);

//MOSTRUO
app.post('/apitechu/v1/mostruo/:p1/:p2',
  function(req, res){ // funcion manejadora
   console.log("POST /apitechu/v1/mostruo/:p1/p2");

    console.log("Parámetros:");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

//GET USERS
app.get('/apitechu/v2/users',userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
app.post('/apitechu/v2/users',userController.createUserV2);
app.put('/apitechu/v2/users/:id',userController.updateUserV2);

//LOGIN
app.post('/apitechu/v2/login',authController.loginV2);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);

//GET ACCOUNTS
app.get('/apitechu/v2/users/:id/accounts',accountsController.getAccountsV2);
app.get('/apitechu/v2/users/:id/accounts/:idac',accountsController.getAccountsByIdV2);
app.delete('/apitechu/v2/users/:id/accounts/:idac',accountsController.deteleAccountV2);
app.post('/apitechu/v2/users/:id/accounts',accountsController.createAccountV2);
app.get('/apitechu/v2/users/:id/accounts/:idac/movements',accountsController.getMovementsV2);
app.post('/apitechu/v2/users/:id/accounts/:idac/movements',accountsController.createMovementV2);

//EXTERNAL
app.get('/apitechu/v2/external/weather',externalAPIsController.getAemetMadridWeatherV2);
