const utils = require('../utils');
const shortid = require('shortid');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=GPWQjLzdeSrm_lHclDEyx345ZWH7LOeL";
const requestJson = require('request-json');

function getAccountsV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Comprobando si el usuario está logeado");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "502",
          "msg" : "Error obteniendo el usuario"
        };
        res.status(502);
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "code" : "403",
          "msg" : "Usuario no autenticado"
        };
        res.status(403);
        res.send(response);
      } else if(body.length > 0 && body[0].logged===true) {
        var httpClient = requestJson.createClient(baseMLabURL);
        var query = 'q={"userId" : ' + id + '}';
        console.log("Buscando cuentas de usuario");
        console.log(baseMLabURL+"cuentas?" + query +"&"+ mLabAPIkey);
        httpClient.get("cuentas?"+ query +"&"+ mLabAPIkey,
          function(err,resMLab, body) {
            if(err){
              var response={
                "code" : "500",
                "msg" : "Error obteniendo cuentas"
              };
              res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                var response={
                  "code" : "404",
                  "msg" : "No se han encontrado cuentas asocias a este usuario"
                };
                res.status(404);
              }
            }
            res.send(response);
          }
        );
      }
    }
  );
}


function getAccountsByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts/:idac");

  var id = req.params.id;
  var idac = req.params.idac;
  var query = 'q={"$and" : [{"userId" : ' + id + '},'+'{"accountId" : "'+ idac + '"}]}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Buscando cuenta de usuario por ID");
  console.log(baseMLabURL+"cuentas?" + query +"&"+ mLabAPIkey);
  httpClient.get("cuentas?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo cuentas"
        };
        res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          var response={
            "code" : "404",
            "msg" : "Cuenta no encontrada"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function deteleAccountV2(req, res) {
  console.log("DELETE /apitechu/v2/users/:id/accounts/:idac");

  var id = req.params.id;
  var idac = req.params.idac;
  var query = 'q={"$and" : [{"userId" : ' + id + '},'+'{"accountId" : "'+ idac + '"}]}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Buscando cuenta de usuario para borrar");
  console.log(baseMLabURL+"cuentas?" + query +"&"+ mLabAPIkey);
  httpClient.get("cuentas?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo cuentas"
        };
        res.status(500);
        res.send(response);
      } else {
        if(body.length > 0) {
          id = body[0]._id.$oid;
          var httpClient = requestJson.createClient(baseMLabURL);
          var querydelete = '/' + id + '';
          console.log("Borrando cuenta de usuario");
          console.log(baseMLabURL+"cuentas" + querydelete +"?"+ mLabAPIkey);
          httpClient.delete("cuentas"+ querydelete +"?"+ mLabAPIkey,
            function(err,resMLab, body) {
              if(err){
                var response={
                  "code" : "500",
                  "msg" : "Error obteniendo cuentas"
                };
                res.status(500);
              } else {
                response = body;
                console.log (response.userId);
              }
              res.send(response);
            }
          );

        } else {
          var response={
            "code" : "404",
            "msg" : "Cuenta no encontrada"
          };
          res.status(404);
          res.send(response);
        }
      }

    }
  );
}

function createAccountV2(req, res){
  console.log("POST /apitechu/v2/users/:id/accounts");
  var id = req.params.id;

  var ibanFormated = utils.generateIBAN(1000, 9999);
  var newAccount = {
    "userId" : parseInt(req.params.id),
    "accountId" : shortid.generate(),
    "IBAN" : ibanFormated,
    "alias" : req.body.alias,
    "balance" : req.body.amount,
  };

  var query = 'q={"id" : ' + id + '}';
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Comprobando si el usuario está logeado");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo el usuario"
        };
        res.status(502);
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "code" : "403",
          "msg" : "Usuario no autenticado"
        };
        res.status(403);
        res.send(response);
      } else if(body.length > 0 && body[0].logged===true) {
          var httpClient = requestJson.createClient(baseMLabURL);
          var query = 'q={"userId" : ' + id + '}';
          console.log("Creando Cuenta");
          console.log(baseMLabURL+"cuentas?"+ query +"&"+ mLabAPIkey);
          httpClient.post("cuentas?" + query +"&"+ mLabAPIkey, newAccount,
            function(err,resMLab, body) {
              console.log("Cuenta Creada!!");
              res.send({"msg" : "Cuenta: ("+newAccount.alias+") creada correctamente, su IBAN es "+newAccount.IBAN+".", "id":newAccount.accountId})
            }
          );
      }
    }
  );
}

function getMovementsV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts/:idac/movements");

  var id = req.params.id;
  var idac = req.params.idac;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Comprobando si el usuario está logeado");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "502",
          "msg" : "Error obteniendo el usuario"
        };
        res.status(502);
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "code" : "403",
          "msg" : "Usuario no autenticado"
        };
        res.status(403);
        res.send(response);
      } else if(body.length > 0 && body[0].logged===true) {
          var httpClient = requestJson.createClient(baseMLabURL);
          var query = 'q={"$and" : [{"userId" : ' + id + '},'+'{"accountId" : "'+ idac + '"}]}';
          console.log("Buscando movimientos para la cuenta seleccionada");
          console.log(baseMLabURL+"movimientos?" + query +"&"+ mLabAPIkey);
          httpClient.get("movimientos?"+ query +"&"+ mLabAPIkey,
            function(err,resMLab, body) {
              if(err){
                var response={
                  "code" : "500",
                  "msg" : "Error obteniendo los movimientos"
                };
                res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  var response={
                    "code" : "404",
                    "msg" : "Movimientos no encontrados para esta cuenta"
                  };
                  res.status(404);
                }
              }
              res.send(response);
            }
        );
      }
    }
  );
}

function createMovementV2(req, res) {
  console.log("POST /apitechu/v2/users/:id/accounts/:idac/movements");
  var query = 'q={"$and" : [{"userId" : ' + req.params.id + '},'+'{"accountId" : "' + req.params.idac + '"}]}';

  var newMovement =
  {
    "userId" : parseInt(req.params.id),
    "accountId" : req.params.idac,
    "movementId" : shortid.generate(),
    "type" : req.body.type,
    "description" : req.body.description,
    "date" : req.body.date,
    "amount" : req.body.amount,
    "balance" : 0
  };

  console.log(newMovement);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Creando movimiento");

  var newBalance = 0;

  console.log("GET Balance="+baseMLabURL+"cuentas?" + query +"&"+ mLabAPIkey);
  httpClient.get("cuentas?"+ query +"&"+ mLabAPIkey,
    function(err, resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo cuenta para balance"
        };
        res.status(500);
        res.send(response);
      } else {
        if(body.length > 0) {
          newBalance = parseFloat(body[0].balance);
          if(newMovement.type === "Ingreso"){
            newBalance = newBalance + parseFloat(newMovement.amount);
          } else if (newMovement.type === "Reintegro"){
            newBalance = newBalance - parseFloat(newMovement.amount);
          }
          if(newBalance < 0){
            var response={
              "code" : "501",
              "msg" : "Saldo negativo no permitido. Vuelva a intentarlo con un importe inferior al saldo de la cuenta."
            };
            res.status(501);
            res.send(response);
          } else{
            newMovement.balance=newBalance.toFixed(2);
            console.log("Creación de movimiento POST Movimientos");
            console.log(baseMLabURL+"movimientos?"+mLabAPIkey);
            httpClient.post("movimientos?"+mLabAPIkey, newMovement,
              function(err,resMLab, body) {
                console.log("Movement Created");
                console.log("PUT Balance="+baseMLabURL+"cuentas?" + query +"&"+ mLabAPIkey);
                  var putBody = '{"$set":{"balance":'+newMovement.balance+'}}';
                  httpClient.put("cuentas?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
                    function(errPUT, resMLabPUT, bodyPUT) {
                      console.log("Actualización del balace HECHO = "+newMovement.balance);
                      var response = {"msg" : "Moviemiento creado y saldo actualizado: "+newMovement.balance}
                      res.send(response);
                    }
                  );
              }
            );
          }
        } else {
          var response={
            "code" : "404",
            "msg" : "Cuenta para actualizar obtener balance no encontrada"
          };
          res.status(404);
          res.send(response);
        }
      }
    }
  )
}

module.exports.getAccountsV2 = getAccountsV2;
module.exports.createAccountV2 = createAccountV2;
module.exports.getAccountsByIdV2 = getAccountsByIdV2;
module.exports.getMovementsV2 = getMovementsV2;
module.exports.createMovementV2 = createMovementV2;
module.exports.deteleAccountV2 = deteleAccountV2;
