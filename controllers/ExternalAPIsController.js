//concatenación de formación de URL
const baseAEMETURL = "https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/28079/";
const aematAPIkey = "api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjYXJsb3MuY2FyYXNhQGdtYWlsLmNvbSIsImp0aSI6ImFlYzcwMjU5LTY2OTQtNGUzNS1hNzE4LWQwYjU5NzNkZDU0MyIsImlzcyI6IkFFTUVUIiwiaWF0IjoxNTMwMjE4NjQ1LCJ1c2VySWQiOiJhZWM3MDI1OS02Njk0LTRlMzUtYTcxOC1kMGI1OTczZGQ1NDMiLCJyb2xlIjoiIn0.a9SkJfgnilA5CPPSoTvUndARhjDSVLVHqqd3_M7mqT4";
const requestJson = require('request-json');


function getAemetMadridWeatherV2(req, res) {
  console.log("GET /apitechu/v2/external/weather");

  var httpClient = requestJson.createClient(baseAEMETURL);
  console.log("Solicitando URL de acceso al tiempo");
  console.log(baseAEMETURL+"?"+aematAPIkey);
  httpClient.get("?"+aematAPIkey,
    function(err, resAEMET, body) {
      if(err){
        var response = {
          "code" : "500",
          "msg" : "Error obteniendo URL de datos de tiempo de la AEMET"
        };
        res.status(500);
        res.send(response);
      } else {
        console.log("Body="+body);
        console.log("URL Datos="+body.datos);
        var httpClient = requestJson.createClient(body.datos);
        console.log("Accediendo a prediccion del tiempo");
        httpClient.get("",
          function(err,resMLab, body) {
            if(err){
              var response = {
                "code" : "500",
                "msg" : "Error obteniendo URL de datos de tiempo de la AEMET"
              };
              res.status(500);
            } else {
              var response = body;
            }
            res.send(response);
          }
        );
      }
    }
  );
}

module.exports.getAemetMadridWeatherV2 = getAemetMadridWeatherV2;
