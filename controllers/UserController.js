const io = require('../io');
const crypt = require('../crypt');
const utils = require('../utils');
const shortid = require('shortid');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=GPWQjLzdeSrm_lHclDEyx345ZWH7LOeL";
const requestJson = require('request-json');

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Obteniendo Usuarios");
  console.log(baseMLabURL);
  console.log(baseMLabURL+"users?"+mLabAPIkey);
  httpClient.get("users?"+mLabAPIkey,
    function(err,resMLab, body) {
      var response = ! err ? body : {
        "code" : "500",
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Obteniendo usuario por ID");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo usuarios"
        };
        res.status(500);
      } else {
        if(body.length > 0 && body[0].logged===true) {
          response = body[0];
        } else if (body.length > 0 && body[0].logged != true) {
          var response={
            "code" : "403",
            "msg" : "Usuario no autenticado"
          };
          res.status(403);
        } else {
          var response={
            "code" : "404",
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function createUserV2(req, res){ // funcion manejadora
  console.log("POST /apitechu/v2/users");

  var newUser = {
    "id" : utils.generateRandom (100000, 999999),
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var email = req.body.email;
  var query = 'q={"email" : "' + email + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Comprobanso si es cliente existente");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "500",
          "msg" : "Error obteniendo usuarios"
        };
        res.status(500);
      } else {
        if(body.length != 0) {
          console.log("Body Err:"+body[0]);
          var response={
            "code" : "501",
            "msg" : "Usuario ya existente con el mismo email"
          };
          res.status(501);
        } else {
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("No existe el usuario -> Creando Usuario");
          console.log(baseMLabURL+"getUsersV2?"+ mLabAPIkey);
          httpClient.post("users?" + mLabAPIkey, newUser,
            function(err,resMLab, body) {
              console.log("Usuario Creado");
              var response={
                "code" : "200",
                "msg" : "User Createado con éxito."
              };
            }
          )
        }
      }
      res.send(response);
    }
  );
}

function updateUserV2(req, res) {
  console.log("PUT /apitechu/v2/users/:id");
  console.log("Actualización de Usuario");

  var id = req.params.id;
  var updateUser;

  console.log("PWD"+req.body.password)

  if (req.body.password!=null || req.body.password!=undefined){
    var updateUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    };
  }else{
    var updateUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };
  }

  console.log("updateUser " + JSON.stringify(updateUser));

  var query = 'q={"id" : ' + id + '}';
  console.log("query es " + query);
  console.log("URL:" + baseMLabURL + "users?" + query + "&" + mLabAPIkey);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Comprobando si el usuario está logeado");
  console.log(baseMLabURL+"users?" + query +"&"+ mLabAPIkey);
  httpClient.get("users?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "code" : "502",
          "msg" : "Error obteniendo el usuario"
        };
        res.status(502);
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "code" : "403",
          "msg" : "Usuario no autenticado"
        };
        res.status(403);
        res.send(response);
      } else if(body.length > 0 && body[0].logged===true) {
        console.log("Usuario con email y password correctos, logging in");
        query = 'q={"id" : ' + body[0].id +'}';
        console.log("Query para el PUT de actualización de usuario " + query);
        var putBody = '{"$set":'+JSON.stringify(updateUser)+'}';
        console.log("putBody " + putBody);
        httpClient.put("users?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, body) {
            console.log("PUT realizado");
            var response = {
              "msg" : "Usuario actualizado con éxito.",
              "Body" : body
            }
            res.send(response);
          }
        );
      }
    }
  );
}

module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.updateUserV2 = updateUserV2;
