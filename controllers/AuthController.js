const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=GPWQjLzdeSrm_lHclDEyx345ZWH7LOeL";




function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");
  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email": "' + email + '"}';
  console.log("query es " + query);
  console.log("URL:" + baseMLabURL + "usesr?" + query + "&" + mLabAPIkey);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("users?" + query + "&" + mLabAPIkey,
    function(err, resMLab, body) {
      if (body.length == 0) {
        var response = {
          "code" : "406",
          "msg" : "Login incorrecto, email y/o passsword no encontrados"
        };
        res.status(406);
        res.send(response);
      } else {
        var isPasswordcorrect = crypt.checkPassword(password, body[0].password);
        console.log("Password correct is " + isPasswordcorrect);
        if (!isPasswordcorrect){
          var response = {
            "code" : "406",
            "msg" : "Login incorrecto, email y/o passsword no encontrados"
          };
          res.status(406);
          res.send(response);
        } else {
          console.log("Usuario con email y password correctos, logging in");
          query = 'q={"id" : ' + body[0].id +'}';
          console.log("Query para el PUT de actualización de session " + query);
          console.log("URL PUT:" + baseMLabURL + "usesr?" + query + "&" + mLabAPIkey);
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("users?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT realizado");
              var response = {
                "msg" : "Usuario logado con éxito.",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          );
        }
      }
    }
  );
}

function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");

  var query = 'q={"id": ' + req.params.id + '}';
  console.log("query es " + query);
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("users?" + query + "&" + mLabAPIkey,
  function(err, resMLab, body) {
    if (body.length == 0) {
      var response = {"mensaje" : "Logout incorrecto, usuario no encontrado"};
      res.send(response);
    } else {
      console.log("Usuario encontradon con ese ID, logging out");
      query = 'q={"id" : ' + body[0].id +'}';
      console.log("Query para el PUT de logout " + query);
      console.log("URL PUT:" + baseMLabURL + "usesr?" + query + "&" + mLabAPIkey);
      var putBody = '{"$unset":{"logged":""}}'
      httpClient.put("users?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
        function(errPUT, resMLabPUT, bodyPUT) {
          console.log("PUT done");
          var response = {
            "msg" : "Usuario deslogado",
            "idUsuario" : body[0].id
          }
          res.send(response);
        }
      );
    }
  }
 );
}

module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
