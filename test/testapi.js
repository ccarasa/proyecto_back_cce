const utils = require('../utils');
const shortid = require('shortid');
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server');

describe('Test API GET V2 USUARIO',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test GET Usuario v2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/')
      .end(
        function(err, res,body){
          console.log("Request GET V2 has Finished!!");
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          console.log("Num users="+res.body.length);
          //console.log(res.body);
          res.body.should.be.a("array");
          for(var data of res.body){
            data.should.have.property('_id');
            data.should.have.property('first_name');
          //  data.should.have.property('last_name');
            data.should.have.property('email');
            data.should.have.property('password');
          }
          done();
        }
      )
    }
    )
  }
)

describe('Test API GET USER BY ID V2 ',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test GET User BY ID V2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/42')
      .end(
        function(err, res, body){
          console.log("Request GET User By ID V2 has Finished!!");
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          res.body.email.should.equal('cce1@cce.com');
          done();
        }
      )
    }
    )
  }
)

let user_details_create = {
  "id": utils.generateRandom (100000, 999999),
  "first_name": "FName Test",
  "last_name": "LName_test",
  "email": "email@test.com",
  "password": "cce1234"
};

let user_details_create_err = {
  "id": 5000,
  "first_name": "FName Test",
  "last_name": "LName_test",
  "email": "email@test.com",
  "password": "cce1234"
};

describe('Test API POST V2 CREAR USUARIO ERROR USUARIO Exietente',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test POST Creación de Usuario ya existente v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users')
      .send(user_details_create)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(501);
          //res.body.code.should.equal('200');
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test POST ERROR Creación de Usuario ya existente v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users')
      .send(user_details_create_err)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(501);
          //res.body.code.should.equal('200');
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

let user_details_query = {
  "id": 42,
  "email": "cce1@cce.com",
  "password": "cce1234"
};

let user_details_query2 = {
  "id": 42,
  "email": "cce1@cce.com",
  "password": "1234"
};

describe('Test API POST V2 LOGIN',
  function(){
    it('Test POST LOGIN de Usuario v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/login')
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          res.body.idUsuario.should.equal(user_details_query.id);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test POST LOGIN de Usuario v2 Con Contraseña ERRONEA', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/login')
      .send(user_details_query2)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(406);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

describe('Test API POST V2 LOGOUT',
  function(){
    it('Test POST LOGOUT de Usuario v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/logout/'+user_details_query.id)
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

describe('Test API GET V2 CUENTAS',
  function(){
    it('Test GET Cuentas v2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/'+user_details_query.id+'/accounts')
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test GET Cuentas v2 Usuario sin estar LOGADO', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/'+3+'/accounts')
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(403);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

let newAccount = {
  "userId" : 42,
  "alias" :  "PRUEBA BORRAR",
  "amount" : 100,
};

var accId='1';

describe('Test API POST V2 CUENTAS',
  function(){
    it('Test POST CREAR Cuentas v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users/'+newAccount.userId+'/accounts')
      .send(newAccount)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          console.log("RES ID ="+res.body.id);
          accId=res.body.id;
          done();
        }
      )
    }
  ), it('Test POST CREAR Cuentas v2 ERROR USUARIO NO LOGADO', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users/4/accounts')
      .send(newAccount)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(403);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

describe('Test API GET V2 CUENTA BY ID',
  function(){
    it('Test GET Cuenta By ID v2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/'+user_details_query.id+'/accounts/'+accId)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test GET Cuenta By ID v2 ERROR CUENTA NO ENCONTRADA', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/'+user_details_query.id+'/accounts/1234')
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(404);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

describe('Test API DELETE V2 CUENTA',
  function(){
    it('Test DELETE Cuenta v2', function(done) {
      chai.request('http://localhost:3000')
      .delete('/apitechu/v2/users/'+user_details_query.id+'/accounts/'+accId)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    ), it('Test DELETE Cuenta v2', function(done) {
      chai.request('http://localhost:3000')
      .delete('/apitechu/v2/users/'+user_details_query.id+'/accounts/1234')
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(404);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

describe('Test API GET V2 MOVIMIENTOs',
  function(){
    it('Test GET MOVIMIENTOs v2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/'+user_details_query.id+'/accounts/31d45f6/movements')
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test GET MOVIMIENTOs v2 ERROR USUARIO NO ATENTICADO', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/4/accounts/31d45f6/movements')
      .send(user_details_query)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(403);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)

let movimiento = {
  "type": "Ingreso",
  "description": "Devolución hacienda",
  "date": "2018-08-20",
  "amount": 560,
};

let movimientoErr = {
  "type": "Reintegro",
  "description": "Pago hacienda",
  "date": "2018-08-21",
  "amount": 80000,
};


describe('Test API POST V2 MOVIMIENTOs',
  function(){
    it('Test POST MOVIMIENTOs v2', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users/'+user_details_query.id+'/accounts/31d45f6/movements')
      .send(movimiento)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(200);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
  ), it('Test POST MOVIMIENTOs v2 EROR SALDO NEGATIVO', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v2/users/'+user_details_query.id+'/accounts/31d45f6/movements')
      .send(movimientoErr)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          res.should.have.status(501);
          console.log("RES="+res.body.msg);
          done();
        }
      )
    }
    )
  }
)
